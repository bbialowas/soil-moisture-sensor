#include <Arduino.h>

#ifndef MeasurementQueue_h
#define MeasurementQueue_h

#define DEFAULT_NUM_READINGS 16

class MeasurementQueue
{
    private:
        uint16_t* _measurementArray;
        uint16_t _numberOfReadings;
        uint16_t _workingIndex = 0;
        uint16_t _measurementSum = 0;
        uint16_t _measurementAverage = 0;
    public:
        MeasurementQueue();
        void init(uint16_t initialMeasurement);
        void init(uint16_t initialMeasurement, uint16_t queueSize);
        void AddMeasurement(uint16_t measurement);
        uint16_t GetMeasurement(int measurementIndex);
        uint16_t GetMeasurementAverage();
        uint16_t GetMeasurementSum();
};

#endif