#include <Arduino.h>
#include <U8g2lib.h>
#include <U8x8lib.h>
#include "MeasurementQueue.h"

// Definitions
#define SOIL_READ_PIN A0
#define SOIL_POWER_PIN D6
#define TRIGGER_BUTTON_PIN D5
#define DEBUG_MODE_PIN D7

// Constants
const char LABEL_MOISTURE_LEVEL[] = "Moisture level:";
const char LABEL_CURRENT_MOISTURE[] = "curr moist:";
const char LABEL_AVERAGE_MOISTURE[] = "avg moist:";
const char LABEL_MOISTURE_LOW_ALERT[] = "Moisture Low";
const char LABEL_WATERING_ALERT[] = "Watering";

const uint8_t LINE_CURRENT_MOISTURE = 2;
const uint8_t LINE_AVERAGE_MOISTURE = 4;
const uint8_t LINE_MOISTURE_LOW_ALERT = 6;
const uint8_t LINE_WATERING_ALERT = 7;

const uint32_t MEASUREMENT_DELAY = 1000 * 60 * 10; // 10min
const uint32_t MEASUREMENT_DELAY_DEBUG = 1000 * 1 * 1; // 1s
const uint32_t MEASUREMENT_DELAY_POWERON = 20; // 10ms
const uint32_t MEASUREMENT_DELAY_WATERING = 200; // 200ms

const uint16_t MEASUREMENT_THRESHOLD_LOW = 716;
const uint16_t MEASUREMENT_THRESHOLD_WATER = 660;
//const uint16_t MEASUREMENT_THRESHOLD_LOW_DEBUG = 1024;

const uint16_t MEASUREMENT_THRESHOLD_HIGH = 820;
//const uint16_t MEASUREMENT_THRESHOLD_HIGH_DEBUG = 1024;

const uint16_t MAX_MOISTURE = 1024;

// Global variables
bool debugMode = false;
bool moistureLow = false;
uint32_t measurementDelay;
uint16_t measurementThresholdLow;
uint16_t measurementTHresholdLowWater;
uint16_t measurementThresholdHigh;
uint16_t previousMeasurement;
uint16_t previousAverage;

uint32_t previousMillis = 0;


/**
 * Oled driver Constructor:
 * U8x8 - text only library
 * SSD1306 - driver
 * 128x64 - resolution
 * NONAME - screen vendor
 * HW - hardware interface
 * I2C - I2C interface
 */
U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8;
MeasurementQueue measurements;

void setup() {
    Serial.begin(9600);

    oledSetupInitial();

    pinMode(LED_BUILTIN, OUTPUT);

    //Read mode of working (if D7 connected to GND, then DEBUG MODE)
    pinMode(DEBUG_MODE_PIN, INPUT_PULLUP);
    if (digitalRead(DEBUG_MODE_PIN) == 0)
    {
        measurementThresholdHigh = MEASUREMENT_THRESHOLD_HIGH;
        measurementThresholdLow = MEASUREMENT_THRESHOLD_LOW;
        measurementTHresholdLowWater = MEASUREMENT_THRESHOLD_WATER;
        measurementDelay = MEASUREMENT_DELAY_DEBUG;
        debugMode = true;
        u8x8.drawString(0, 0, "DEBUG MODE");
        Serial.println("DEBUG_MODE");
        delay(1000);
    }
    else
    {
        measurementThresholdHigh = MEASUREMENT_THRESHOLD_HIGH;
        measurementThresholdLow = MEASUREMENT_THRESHOLD_LOW;
        measurementTHresholdLowWater = MEASUREMENT_THRESHOLD_WATER;
        measurementDelay = MEASUREMENT_DELAY;
        u8x8.drawString(0, 0, "NORMAL MODE");
        Serial.println("NORMAL_MODE");
        delay(1000);
    }
    pinMode(DEBUG_MODE_PIN, OUTPUT);
    digitalWrite(DEBUG_MODE_PIN, LOW);

    //Setup pin for powering sensor during measurement
    pinMode(SOIL_POWER_PIN, OUTPUT);
    digitalWrite(SOIL_POWER_PIN, LOW);
    pinMode(SOIL_READ_PIN, INPUT);

    pinMode(LED_BUILTIN, OUTPUT);
    digitalWrite(LED_BUILTIN, HIGH);

    //External interrupt setup
    pinMode(TRIGGER_BUTTON_PIN, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(TRIGGER_BUTTON_PIN), handleInterrupt, FALLING);

    //MeasurementQueue init with current moisture value
    uint16_t initialMoist = readSoilMoist();
    measurements.init(initialMoist, 32);
    previousMeasurement = initialMoist;
    previousAverage = measurements.GetMeasurementAverage();

    oledSetupWork();
    oledUpdateCurrentMoist(previousMeasurement);
    oledUpdateAverageMoist(previousAverage);
}

void loop()
{
    uint32_t currentMillis = millis();
    if(currentMillis - previousMillis >= measurementDelay - MEASUREMENT_DELAY_POWERON)
    {
        previousMillis = currentMillis;

        // read current moisture level and update info on display
        uint16_t currentMoist = readSoilMoist();
        measurements.AddMeasurement(currentMoist);
        if (currentMoist != previousMeasurement)
        {
            previousMeasurement = currentMoist;
            oledUpdateCurrentMoist(currentMoist);
        }
        if (measurements.GetMeasurementAverage() != previousAverage)
        {
            previousAverage = measurements.GetMeasurementAverage();
            oledUpdateAverageMoist(measurements.GetMeasurementAverage());
        }


        if (!moistureLow)
        {
            // print current moisture values if debug
            if (debugMode)
            {
                Serial.println(String(currentMoist) + " " + String(measurements.GetMeasurementAverage()));
            }

            // if moisture level is under watering threshold, turn on LowMoisture mode and decrease delay time
            if (measurements.GetMeasurementAverage() < measurementTHresholdLowWater)
            {
                moistureLow = true;
                measurementDelay = MEASUREMENT_DELAY_WATERING;
                oledDrawLowMoistureWarning();
                oledDrawWateringWarning();
                digitalWrite(LED_BUILTIN, LOW);
            }
            // if moisture level is under threshold, alert
            else if (measurements.GetMeasurementAverage() < measurementThresholdLow)
            {
                oledDrawLowMoistureWarning();
                digitalWrite(LED_BUILTIN, HIGH);
                if(!debugMode)
                {
                    sendAlert();
                }
            }
            else
            {
                oledClearLowMoistureWarning();
                digitalWrite(LED_BUILTIN, HIGH);
            }
        }
        else
        {
            // pump water

            if (measurements.GetMeasurementAverage() > measurementThresholdHigh)
            {
                moistureLow = false;
                measurementDelay = debugMode ? MEASUREMENT_DELAY_DEBUG : MEASUREMENT_DELAY;
                oledClearLowMoistureWarning();
                oledClearWateringWarning();
                digitalWrite(LED_BUILTIN, HIGH);
            }
        }
    }
}

// Measurement functions
uint16_t readSoilMoist()
{
    digitalWrite(SOIL_POWER_PIN, HIGH);
    delay(MEASUREMENT_DELAY_POWERON);
    uint16_t moistValue = analogRead(SOIL_READ_PIN);
    digitalWrite(SOIL_POWER_PIN, LOW);
    return moistValue;
}

// Oled screen functions
void oledSetupInitial()
{
    u8x8.begin();
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setFlipMode(true);
}

void oledSetupWork()
{
    u8x8.clearDisplay();
    u8x8.drawString(0, 0, LABEL_MOISTURE_LEVEL);
    u8x8.drawString(1, 1, LABEL_CURRENT_MOISTURE);
    u8x8.drawString(1, 3, LABEL_AVERAGE_MOISTURE);
}

void oledUpdateAverageMoist(uint16_t averageMoist)
{
    oledUpdateMoist(averageMoist, LINE_AVERAGE_MOISTURE);
}

void oledUpdateCurrentMoist(uint16_t currentMoist)
{
    oledUpdateMoist(currentMoist, LINE_CURRENT_MOISTURE);
}

void oledUpdateMoist(uint16_t moist, uint8_t line)
{
    uint16_t percentValue = (uint16_t)(((float)moist/(float)MAX_MOISTURE) * 100);
    u8x8.clearLine(line);
    u8x8.setCursor(2, line);
    if(percentValue >= 0 && percentValue < 10)
    {
        u8x8.drawString(3, line, "%");
    }
    else if (percentValue >= 10 && percentValue < 100)
    {
        u8x8.drawString(4, line, "%");
    }
    else
    {
        percentValue = 100;
        u8x8.drawString(5, line, "%");
    }
    u8x8.print(percentValue);
}

void oledDrawLowMoistureWarning()
{
    u8x8.drawString(1, LINE_MOISTURE_LOW_ALERT, LABEL_MOISTURE_LOW_ALERT);
}

void oledClearLowMoistureWarning()
{
    u8x8.clearLine(LINE_MOISTURE_LOW_ALERT);
}

void oledDrawWateringWarning()
{
    u8x8.drawString(1, LINE_WATERING_ALERT, LABEL_WATERING_ALERT);
}

void oledClearWateringWarning()
{
    u8x8.clearLine(LINE_WATERING_ALERT);
}

// Interrupt handlers
void handleInterrupt()
{
    Serial.println("Interrupt");
    sendAlert();
}

// Messaging functions
void sendAlert()
{
    Serial.println("MOISTURE_LOW");
}