#include "MeasurementQueue.h"

MeasurementQueue::MeasurementQueue()
{

}

void MeasurementQueue::init(uint16_t initialMeasurement)
{
    init(initialMeasurement, DEFAULT_NUM_READINGS);
}

void MeasurementQueue::init(uint16_t initialMeasurement, uint16_t queueSize)
{
    _numberOfReadings = queueSize;
    if (_measurementArray == 0)
    {
        _measurementArray = (uint16_t*) malloc(_numberOfReadings * sizeof(uint16_t));
    }
    else
    {
        _measurementArray = (uint16_t*) realloc(_measurementArray, _numberOfReadings * sizeof(uint16_t));
    }

    for (int i = 0; i < _numberOfReadings; i++)
    {
        _measurementArray[i] = initialMeasurement;
        _measurementSum += initialMeasurement;
    }
    _measurementAverage = _measurementSum / _numberOfReadings;
}

void MeasurementQueue::AddMeasurement(uint16_t measurement)
{
    _measurementSum -= _measurementArray[_workingIndex];
    _measurementSum += measurement;
    _measurementAverage = _measurementSum / _numberOfReadings;
    _measurementArray[_workingIndex] = measurement;

    _workingIndex++;
    if(_workingIndex >= _numberOfReadings)
    {
        _workingIndex = 0;
    }
}

uint16_t MeasurementQueue::GetMeasurement(int measurementIndex)
{
    if (measurementIndex > _numberOfReadings)
    {
        return 0;
    }
    return _measurementArray[measurementIndex];
}

uint16_t MeasurementQueue::GetMeasurementAverage()
{
    return _measurementAverage;
}

uint16_t MeasurementQueue::GetMeasurementSum()
{
    return _measurementSum;
}